package com.longnguyen.util;

import com.longnguyen.tests.flightreservation.model.FlightReservationTestData;
import com.longnguyen.tests.vendorportal.model.VendorPortalTestData;

public class Demo {
    public static void main(String[] args) {
        FlightReservationTestData testData = JsonUtil.getTestData("test-data/flight-reservation/passenger-1.json", FlightReservationTestData.class);
        System.out.println(testData.firstName());

        VendorPortalTestData testData2 = JsonUtil.getTestData("test-data/vendor-portal/john.json", VendorPortalTestData.class);
        System.out.println(testData2.monthlyEarning());
    }
}
